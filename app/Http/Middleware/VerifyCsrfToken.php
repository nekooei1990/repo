<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
//    protected function tokensMatch($request)
//    {
//        $token = $request->input('_token') ?: $request->header('X-CSRF-TOKEN');
//        $token = $request->ajax()  ? $request->header('X-CSRF-Token') : $request->input('_token');
//        return $request->session()->token() == $token;
//
//        if ( ! $token && $header = $request->header('X-XSRF-TOKEN'))
//        {
//            $token = $this->encrypter->decrypt($header);
//        }
//
//        return StringUtils::equals($request->session()->token(), $token);
//    }

    protected  function tokensMatch($request){
        $token = $request->ajax()  ? $request->header('X-CSRF-Token') : $request->input('_token');
        return $request->session()->token() == $token;
    }
}
