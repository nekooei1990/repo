<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\workplace;
use App\Models\Zone;
use http\Env\Response;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class WorkplaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = workplace::all();
        $zone = Zone::all();
        $index=1;
        return view('adminpanel.workplace.create',['items'=>$items , 'index' =>$index,'zones' =>$zone]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->ajax()){
            $repeatedLocationsAndId = workplace::whereLike('locations',$request->input('addresslist'))
                ->orWhereLike('zone_id',$request->input('zone'))
                ->first();

        if( !$repeatedLocationsAndId){
            $workplace = new workplace();
                $workplace->locations = $request->input('addresslist');
                $workplace->zone_id = $request->input('zone');
                $workplace->city_id   = $request->input('city');
                $workplace->state_id  = $request->input('state');
                $workplace->save();
                return response()->json([
                                'success' =>  'ok','msg' => 'درج با موفقیت انجام شد'
                ]);

        }else{

            return response()->json([
                                    'success' =>  'NOT','msg'  =>  'چنین داده ای وجود دارد!' ,
                                    'id' => $repeatedLocationsAndId->id
            ]);
        }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateEditable(Request $request)
    {
        if ($request->ajax()) {
            $value ='';
            if($request->input('name') == 'status'){//check for status
                if($request->input('value')==1){
                    $value = 'active';
                }elseif($request->input('value')==2){
                    $value ='disable';
                }
                $wp = workplace::find($request->input('pk'));
                $wp->status = $value;
                $wp->save();
                return response()->json(['success' => true ,'data' =>$value]);
            }else{
                //check for address
                workplace::find($request->input('pk'))
                    ->update([
                        $request->input('name') => $request->input('value')
                    ]);
                return response()->json(['success' => true]);
            }
        }
    }
    public function updateZone(Request $request)
    {
        if ($request->ajax()) {
            $workplace =workplace::find($request->input('pk'));
            $city_id = $workplace->city_id;
            $new_zone_id = $request->input('value');
            $dupliacted =workplace::where('city_id',$city_id)->where('zone_id',$new_zone_id)->first();

            if($dupliacted){
            //If Duplicated zone and city Don't Allow to update fields

                return response()->json(['error' => true,'flag'=>true,'message'=>'Duplicated Zone In One City'.'امکان ویرایش وجودندارد']);
            }
            $workplace->zone_id = $request->input('value');
                $workplace->save();
            return response()->json(['success' => true,'value'=>$request->input('value')]);
        }
    }
}
