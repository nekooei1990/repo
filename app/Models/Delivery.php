<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    public function workplace()
    {
        return $this->belongsTo(workplace::class);
    }

    //local Scope
    //you can use this method in your eloqent relation with
    // Delivery::WhereLike('email','$email')->OrWhereLike('name','$name')
    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
}
