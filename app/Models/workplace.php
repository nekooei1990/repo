<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class workplace extends Model
{
    use HasFactory;
    protected $fillable =['locations','id'];



    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }

    protected static function booted()
    {
        //

    }
    //
    //local Scope
    //
        public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
    //local Scope
    public function scopePopular($query)
    {
        return $query->where('votes', '>', 100);
    }

    //you can use in your code by This examples
    //determine ScopeAvtice()in your Model But, In your controller code uses active()
    //without "scope" phrase (Word)
    //$users = User::popular()->active()->orderBy('created_at')->get();

//    public function scopePublished($query)
//    {
//        return $query->whereNotNull('published_at');
//    }
//
//    public function scopeDraft($query)
//    {
//        return $query->whereNull('published_at');
//    }


    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeStatus($query ,$status)
    {
        return $query->where('status',$status);
        //return $query->where('status','=',1); //if the tables's field of detabase is Integer
    }
    // workplace::find(1)->status('active');
    public function scopePublished($query)
    {
        return $query->where('published', true);
    }

    //$publishedNewsPosts = Post::published()->type('news')->get();
    //
    //$draftSpecialOfferPosts = Post::draft()->type('special_offers')->get();

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%'.$value.'%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%'.$value.'%');
    }
}
