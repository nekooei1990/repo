@extends('adminpanel.layout.master')

@section('content')
<div class="box">

    <div class="box-header with-border">
        <h3 class="box-title">Title</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body" style="">
        <div class="alert alert-dismissible displayNone" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> توجه!!!!</h4>
            <p id="dataMSG">

            </p>
        </div>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ثبت مناطق</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="POST"  action="#">
                    @csrf
                    <!--<input type="hidden" name="_token" value="{{ csrf_token() }}" />-->
                    <div class="box-body">
                        <div class="form-group col-sm-6">
                            <label for="loactions">
                               در این قسمت آدرس ها را بر اساس "کاما" جدا کنید و در انتها منطقه مورد نظر را انتخاب نمایید
                            </label>
                            <textarea id="addresslist" name="addresslist" class="form-control" rows="3" placeholder="شهرک گلستان،بلوار صنایع،مطهری،"></textarea>
                        </div>
                        <div class="form-group col-sm-2">
                            <label>
                                ناحیه
                            </label>
                            <select id="zone" name="zone" class="form-control">
                                @foreach($zones as $zone)
                                    <option value="{{$zone->id}}">{{$zone->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-2">
                            <label>
                                استان
                            </label>
                            <select id="state" name="state" class="form-control">
                                    <option value="">انتخاب کنید</option>
                                    <option value="2">فارس</option>
                                    <option value="3">تهران</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-2">
                            <label>
                                شهر
                            </label>
                            <select id="city" name="city" class="form-control">
                                    <option value="">انتخاب کنید</option>
                                    <option value="2">شیراز</option>
                                    <option value="3">تهران</option>
                            </select>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer col-sm-4 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary btn-block">درج </button>
                    </div>
                </form>
            </div>
            <!-- /.box -->


        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="">

    </div>
    <!-- /.box-footer-->
</div>

    <!-- table-->
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
لیست آدرس ها بر اساس مناطق
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="dataTables_length" id="example1_length">
                                <label>نمایش
                                    <select name="example1_length" aria-controls="example1" class="form-control input-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> تعداد</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div id="example1_filter" class="dataTables_filter">
                                <label>جستجو
                                    <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 182.467px;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                    ردیف
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 225.017px;" aria-label="Browser: activate to sort column ascending">
                                        لیست آدرس ها
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 225.017px;" aria-label="Browser: activate to sort column ascending">
                                        ناحیه
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 225.017px;" aria-label="Browser: activate to sort column ascending">
                                        استان
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 225.017px;" aria-label="Browser: activate to sort column ascending">
                                        شهر
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 225.017px;" aria-label="Browser: activate to sort column ascending">
                                        وضعیت
                                    </th>


                                </tr>
                                </thead>
                                <tbody>

                                @foreach($items as $item)

                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        @csrf
                                    <tr role="row" class="odd" id="{{$item->zone_id}}">

                                        <td class="sorting_1">{{$index++}}</td>
                                        <td>
                                            <a href="#" class="address" data-type="text"
                                               data-pk="{{$item->id}}" data-rows="1"
                                               data-name="locations">
                                                {{$item->locations}}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#" class="zone" data-type="select"
                                               data-value="{{$item->zone_id}}" data-pk="{{$item->id}}"
                                               data-title="Select status" data-name="zone">
                                            </a>
                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <a href="#" class="status" data-type="select"
                                               data-value="{{($item->status == 'active'?1:2)}}"
                                               data-pk="{{$item->id}}"
                                               data-title="Select status" data-name="status">
                                            </a>
                                        </td>
                                    <tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                <ul class="pagination">
                                    <li class="paginate_button previous disabled" id="example1_previous"><a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                                    <li class="paginate_button active"><a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a></li>
                                    <li class="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a></li>
                                    <li class="paginate_button next" id="example1_next"><a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
@endsection
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }
        });


        $(document).ready(function(){

            $('.zone').editable({
                url: '{{url("workplace/updateZone")}}',
                source: [
                        @foreach($zones as $item)
                    {
                        value: {{$item->id}},
                        text: '{{$item->title}}'
                    },
                    @endforeach
                ],
                success: function (response, newValue) {
                    console.log('Updated', response.message)
                    if(response.flag)
                        alert(response.message)
                }
            });

            $('.address').editable({
                url: '{{url("workplace/updateEditable")}}',
                title: 'ویرایش',
                success: function (response, newValue) {
                    console.log('Updated', response)
                }
            });

            $('.status').editable({
                url: '{{url("workplace/updateEditable")}}',
                title: 'ویرایش',
                source:[
                    {'value':1,'text':'فعال'},
                    {'value':2,'text':'غیر فعال'}
                ],
                success: function (response, newValue) {
                    console.log('Updated', response)
                }
            });

            $('form').submit(function(e){
                e.preventDefault();
                console.log($(this).serialize())

                // let formdata= JSON.parse($(this).serialize())
                $.ajax({
                    url: "{{ url(''.route('workplace.store')) }}",
                    method: 'POST',
                    dataType: "json",
                    data: {
                       zone:$('#zone').val(),
                        addresslist:$('#addresslist').val(),
                        city:$('#city').val(),
                        state:$('#state').val()
                    },
                    success: function(result){
                        if(result.success == 'ok'){
                            //if insert succesfully
                            $('.alert').addClass('displayNone').removeClass('alert-danger')
                            $('.alert').addClass('alert-success').removeClass('displayNone')
                            $('#dataMSG').text(result.msg)
                        }else {
                            //if insert not succesfully and you must to update fields
                            var id = result.id
                            $('tr').removeClass('highlight')
                            $('.alert').addClass('displayNone').removeClass('alert-success')
                            $('.alert').addClass('alert-danger').removeClass('displayNone')
                            $('#dataMSG').text(result.msg)

                            $('tr#'+id).addClass('highlight')
                        }

                    }});
            });
        });
    </script>
@endsection
