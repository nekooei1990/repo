@extends('adminpanel.layout.master')

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Title</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body" style="">
        <div class="col-md-offset-2 col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ثبت مناطق</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">
                               در این قسمت آدرس ها را بر اساس "کاما" جدا کنید و در انتها منطقه مورد نظر را انتخاب نمایید
                            </label>
                            <textarea class="form-control" rows="3" placeholder="شهرک گلستان،بلوار صنایع،مطهری،"></textarea>

                        </div>
                        <div class="form-group">
                            <label>
                                منطقه
                            </label>
                            <select class="form-control">
                                <option>منطقه 1</option>
                                <option>منطقه 2</option>
                            </select>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">ذخیره</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->


        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="">

    </div>
    <!-- /.box-footer-->
</div>
@endsection
