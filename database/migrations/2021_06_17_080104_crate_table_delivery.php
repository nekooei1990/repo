<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrateTableDelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->integer('courier_id')->unsigned();//deliver man
            $table->integer('customer_id')->unsigned();
            $table->integer('seller_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->unsignedInteger('workplace_id')
                ->index()
                ->foreign('workplace_id')->references('id')->on('workplaces');
            $table->integer('delivery_item_id')->unsigned();
            $table->integer('quantity_item')->unsigned();
            $table->integer('priority_id')->unsigned();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
