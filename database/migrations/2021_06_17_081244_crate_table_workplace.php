<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrateTableWorkplace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {//this table is for set a location for delivery's send or receive packets it mean you determine a sets(group) of location
        // that courier(delivery man) can send or receive packets from store or customer
        Schema::create('workplaces', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('zone_id')
                ->index()
                ->foreign('zone_id')->references('id')->on('zones');//locations id (Set's of locations has one id that is parish_id)
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('state_id');
            $table->string('locations');
            $table->string('status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('workplaces');
//        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
