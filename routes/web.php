<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Backend\PanelController;
use App\Http\Controllers\Backend\WorkplaceController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('panel',PanelController::class);
Route::resource('workplace',WorkplaceController::class);
Route::post('workplace/updateEditable', [workplaceController::class,'updateEditable']);
Route::post('workplace/updateZone', [workplaceController::class,'updateZone']);

